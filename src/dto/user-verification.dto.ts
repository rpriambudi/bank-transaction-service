export class UserVerificationDto {
    userId: number;
    challenge: string;
}