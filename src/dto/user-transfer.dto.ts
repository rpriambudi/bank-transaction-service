import { BaseTransferDto } from './base-transfer.dto';
import { UserVerificationDto } from './user-verification.dto';

export class UserTransferDto extends BaseTransferDto {
    clientType: string;
    cifNo:string;
    userVerificationDto: UserVerificationDto;
}