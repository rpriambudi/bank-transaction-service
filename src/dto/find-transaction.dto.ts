export class FindTransactionDto {
    transactionType: string;
    dateFrom: string;
    dateTo: string;
}