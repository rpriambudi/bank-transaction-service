import { IsNotEmpty } from 'class-validator';

export class BaseTransferDto {
    @IsNotEmpty()
    amount: number;

    @IsNotEmpty()
    destinationAcctNo: string;

    requesterAcctNo: string;

    branchCreated: string;

    createdBy: string;
    
    description: string;
}