import { IsNotEmpty, IsPositive } from 'class-validator';

export class SavingAdjusmentDto {
    @IsNotEmpty()
    @IsPositive()
    amount: number;

    accountNo: string;
    
    branchCode: string;

    createdBy: string;
}