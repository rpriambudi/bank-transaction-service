import { BaseEvent } from 'bank-shared-lib';

export class AddDestinationBalanceFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'addDestinationBalanceFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}