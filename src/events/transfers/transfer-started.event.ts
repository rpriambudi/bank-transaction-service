import { BaseEvent } from 'bank-shared-lib';

export class TransferStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'transferStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}