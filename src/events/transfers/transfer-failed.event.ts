import { BaseEvent } from 'bank-shared-lib';

export class TransferFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'transferFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}