import { BaseEvent } from 'bank-shared-lib';

export class TransferSuccessEvent extends BaseEvent {
    getEventName(): string {
        return 'transferSuccessEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}