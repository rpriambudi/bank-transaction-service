import { BaseEvent } from 'bank-shared-lib';

export class RequesterBalanceRollbackedEvent extends BaseEvent {
    getEventName(): string {
        return 'requesterBalanceRollbackedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}