import { BaseEvent } from 'bank-shared-lib';

export class TransferCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'transferCreatedEvent';
    }

    getUpdatedData(stateData: any, eventData: any) {
        stateData.transactionId = eventData.transactionId;
        return stateData;
    }
    
}