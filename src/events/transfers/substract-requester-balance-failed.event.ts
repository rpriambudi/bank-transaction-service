import { BaseEvent } from 'bank-shared-lib';

export class SubstractRequesterBalanceFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'substractRequesterBalanceFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}