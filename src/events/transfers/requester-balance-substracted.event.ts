import { BaseEvent } from 'bank-shared-lib';

export class RequesterBalanceSubstractedEvent extends BaseEvent {
    getEventName(): string {
        return 'requesterBalanceSubstractedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}