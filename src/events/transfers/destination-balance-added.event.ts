import { BaseEvent } from 'bank-shared-lib';

export class DestinationBalanceAddedEvent extends BaseEvent {
    getEventName(): string {
        return 'destinationBalanceAddedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}