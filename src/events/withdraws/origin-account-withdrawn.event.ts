import { BaseEvent } from 'bank-shared-lib';

export class OriginAccountWithdrawnEvent extends BaseEvent {
    getEventName(): string {
        return 'originAccountWithdrawnEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}