import { BaseEvent } from 'bank-shared-lib';

export class WithdrawalCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'withdrawalCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.transactionId = eventData.transactionId;
        return stateData;
    }
    
}