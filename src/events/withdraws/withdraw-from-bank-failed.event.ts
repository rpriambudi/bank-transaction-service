import { BaseEvent } from 'bank-shared-lib';

export class WithdrawFromBankFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'withdrawFromBankFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}