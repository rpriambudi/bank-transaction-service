import { BaseEvent } from 'bank-shared-lib';

export class WithdrawalFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'withdrawalFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}