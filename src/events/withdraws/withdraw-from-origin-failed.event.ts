import { BaseEvent } from 'bank-shared-lib';

export class WithdrawFromOriginFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'withdrawFromOriginFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}