import { BaseEvent } from 'bank-shared-lib';

export class BankAccountWithdrawnEvent extends BaseEvent {
    getEventName(): string {
        return 'bankAccountWithdrawnEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}