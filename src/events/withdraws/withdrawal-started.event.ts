import { BaseEvent } from 'bank-shared-lib';

export class WithdrawalStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'withdrawalStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}