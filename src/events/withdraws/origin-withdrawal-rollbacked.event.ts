import { BaseEvent } from 'bank-shared-lib';

export class OriginWithdrawalRollbackedEvent extends BaseEvent {
    getEventName(): string {
        return 'originWithdrawalRollbackedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}