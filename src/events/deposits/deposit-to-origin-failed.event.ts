import { BaseEvent } from 'bank-shared-lib';

export class DepositToOriginFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'depositToOriginFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}