import { BaseEvent } from 'bank-shared-lib';

export class DepositStartedEvent extends BaseEvent {
    getEventName(): string {
        return 'depositStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}