import { BaseEvent } from 'bank-shared-lib';

export class OriginAccountDepositedEvent extends BaseEvent {
    getEventName(): string {
        return 'originAccountDepositedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}