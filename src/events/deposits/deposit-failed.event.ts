import { BaseEvent } from 'bank-shared-lib';

export class DepositFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'depositFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}