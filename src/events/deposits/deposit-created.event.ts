import { BaseEvent } from 'bank-shared-lib';

export class DepositCreatedEvent extends BaseEvent {
    getEventName(): string {
        return 'depositCreatedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.transactionId = eventData.transactionId;
        return stateData;
    }
    
}