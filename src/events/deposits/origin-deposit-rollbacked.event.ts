import { BaseEvent } from 'bank-shared-lib';

export class OriginDepositRollbackedEvent extends BaseEvent {
    getEventName(): string {
        return 'originDepositRollbackedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}