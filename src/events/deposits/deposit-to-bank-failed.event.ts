import { BaseEvent } from 'bank-shared-lib';

export class DepositToBankFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'depositToBankFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.errorMessage = eventData.error;
        return stateData;
    }
    
}