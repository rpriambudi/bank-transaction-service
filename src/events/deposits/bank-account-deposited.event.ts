import { BaseEvent } from 'bank-shared-lib';

export class BankAccountDepositedEvent extends BaseEvent {
    getEventName(): string {
        return 'bankAccountDepositedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}