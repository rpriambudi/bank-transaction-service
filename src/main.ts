import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IBrokerConsumer } from 'bank-shared-lib';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const sagaConsumer = app.select(AppModule).get<IBrokerConsumer>('SagaExecutor');
  const commandConsumer = app.select(AppModule).get<IBrokerConsumer>('BrokerConsumer');
  sagaConsumer.consume();
  commandConsumer.consume();
  await app.listen(3005);
}
bootstrap();
