import { SavingAdjusmentDto } from './../../dto/saving-adjustmernt.dto';
import { BaseTransferDto } from './../../dto/base-transfer.dto'; 
import { FindTransactionDto } from './../../dto/find-transaction.dto';
import { Transaction } from './../../entities/transaction.entity';

export interface TransactionService {
    createNewTransfer(transferDto: BaseTransferDto): Promise<Transaction>;
    requestWithdrawal(adjustmentDto: SavingAdjusmentDto): Promise<Transaction>;
    requestDeposit(adjustmentDto: SavingAdjusmentDto): Promise<Transaction>;
    setTransactionToFail(transactionId: number): Promise<Transaction>;
    setTransactionToSuccess(transactionId: number): Promise<Transaction>;
    findTransactionByCif(cifNo: string, findDto: FindTransactionDto): Promise<Transaction[]>;
    findTransactionByAccount(accountNo: string, findDto: FindTransactionDto): Promise<Transaction[]>;
}