import { AuthMiddleware } from 'bank-shared-lib';

export class CustomerAuthMiddleware extends AuthMiddleware {
    getStrategyName(): string {
        return 'JWT';
    }    
    
    getGuards(): string[] {
        return ['Customer'];
    }

}