import { AuthMiddleware } from 'bank-shared-lib';

export class TellerAuthMiddleware extends AuthMiddleware {
    getStrategyName(): string {
        return 'JWT';
    }    
    
    getGuards(): string[] {
        return ['Teller'];
    }

}