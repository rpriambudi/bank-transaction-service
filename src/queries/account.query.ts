import axios from 'axios';
import { ConfigService } from '@nestjs/config';

export class AccountQuery {
    constructor(private readonly configService: ConfigService) {}

    async getAccountsByCustomerId(customerId: number): Promise<[]> {
        const accountUrl = this.configService.get<string>('account.serviceUrl');
        const { data } = await axios.get(`${accountUrl}/api/accounts/${customerId}/customer`);
        if (!data)
            return [];

        return data;
    }
}