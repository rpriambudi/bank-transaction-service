import axios from 'axios';
import { ConfigService } from '@nestjs/config';

export class CustomerQuery {
    constructor(private readonly configService: ConfigService) {}

    async getCustomerByCifNo(cifNo: string): Promise<any> {
        const customerUrl = this.configService.get<string>('customer.serviceUrl');
        const { data } = await axios.get(`${customerUrl}/api/customer/${cifNo}`);
        return data;
    }
}