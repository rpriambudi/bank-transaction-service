import { BusinessRule } from './../interfaces/business-rule.interface';

export class RuleContext implements BusinessRule {
    constructor(
        private readonly ruleList: BusinessRule[]
    ) {}

    async validate(validationData: any): Promise<void> {
        for (const rule of this.ruleList) {
            await rule.validate(validationData);
        }
    }
}