import { BusinessRule } from './../../interfaces/business-rule.interface';
import { BaseTransferDto } from './../../dto/base-transfer.dto';
import { SameAccountException } from './../../exceptions/same-account.exception';

export class TransferAccountRule implements BusinessRule {
    async validate(createTransferDto: BaseTransferDto) {
        this.sameAccount(createTransferDto.requesterAcctNo, createTransferDto.destinationAcctNo);
    }

    private sameAccount(requesterAcctNo: string, destinationAcctNo: string): void {
        if (requesterAcctNo === destinationAcctNo)
            throw new SameAccountException('Same account transfer is prohibited by system.')
        return;
    }
}