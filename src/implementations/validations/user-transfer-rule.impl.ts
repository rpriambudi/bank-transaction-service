import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { BusinessRule } from './../../interfaces/business-rule.interface';
import { UserAccountDto } from './../../dto/user-account.dto';
import { UserAccountNotExist } from './../../exceptions/user-account-not-exist.exception';

export class UserTransferRule implements BusinessRule {
    constructor(
        private readonly configService: ConfigService
    ) {}

    async validate(userAccountDto: UserAccountDto): Promise<void> {
        const userAccounts = await this.getAccountData(userAccountDto.customerId);
        if (!userAccounts || userAccounts.length === 0)
            throw new UserAccountNotExist('Customer does not have any account.');

        let exist = false;
        for (const userAccount of userAccounts) {
            if (userAccount.accountNo === userAccountDto.customerAcctNo) {
                exist = true;
                break;
            }
        }

        if (!exist)
            throw new UserAccountNotExist('Customer account does not match.');
    }

    private async getAccountData(customerId: number) {
        const accountServiceUrl = `${this.configService.get<string>('account.serviceUrl')}/api/accounts/${customerId}/customer`
        const { data } = await axios.get(accountServiceUrl);
        return data;
    }

}