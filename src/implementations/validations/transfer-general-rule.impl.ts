import { Injectable } from '@nestjs/common';
import { BusinessRule } from './../../interfaces/business-rule.interface';
import { BaseTransferDto } from './../../dto/base-transfer.dto';
import { NegativeAmountException } from './../../exceptions/negative-amount.exception';

@Injectable()
export class TransferGeneralRule implements BusinessRule {
    
    async validate(createTransferDto: BaseTransferDto): Promise<void> {
        this.negativeAmount(createTransferDto.amount);
    }

    private negativeAmount(amount: number): void {
        if (amount <= 0)
            throw new NegativeAmountException('Amount is insufficient. Negative amount.');
        return;
    }
}
