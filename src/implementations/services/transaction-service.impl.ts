import * as uuid from 'uuid';
import axios from 'axios';
import { Repository, SelectQueryBuilder, getRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { Transaction, TransactionTypeEnum, TransactionStatus } from './../../entities/transaction.entity';
import { TransactionService } from './../../interfaces/services/transaction-service.interface';
import { BaseTransferDto } from './../../dto/base-transfer.dto';
import { SavingAdjusmentDto } from './../../dto/saving-adjustmernt.dto';
import { FindTransactionDto } from './../../dto/find-transaction.dto';

export class TransactionServiceImpl implements TransactionService {
    constructor(
        @InjectRepository(Transaction) private readonly repository: Repository<Transaction>,
        private readonly configService: ConfigService
    ) {}

    async createNewTransfer(userTransferDto: BaseTransferDto): Promise<Transaction> {
        const transaction = this.repository.create(userTransferDto);
        transaction.transactionId = uuid.v4();
        transaction.transactionType = TransactionTypeEnum.Transfer;

        return await this.repository.save(transaction);
    }

    async requestWithdrawal(adjustmentDto: SavingAdjusmentDto): Promise<Transaction> {
        const transaction = this.createAdjustmentTransactionData(adjustmentDto);
        transaction.transactionType = TransactionTypeEnum.Withdraw;

        return await this.repository.save(transaction);
    }

    async requestDeposit(adjustmentDto: SavingAdjusmentDto): Promise<Transaction> {
        const transaction = this.createAdjustmentTransactionData(adjustmentDto);
        transaction.transactionType = TransactionTypeEnum.Deposit;

        return await this.repository.save(transaction);
    }

    async setTransactionToFail(transactionId: number): Promise<Transaction> {
        const transaction = await this.repository.findOne({ id: transactionId });
        transaction.transactionStatus = TransactionStatus.Failed;
        
        return await this.repository.save(transaction);
    }

    async setTransactionToSuccess(transactionId: number): Promise<Transaction> {
        const transaction = await this.repository.findOne({ id: transactionId });
        transaction.transactionStatus = TransactionStatus.Settled;
        return await this.repository.save(transaction);
    }

    async findTransactionByCif(cifNo: string, findDto: FindTransactionDto): Promise<Transaction[]> {
        const queryBuilder = this.startQueryBuilder(findDto);
        const accounts = await this.getCustomerAccounts(cifNo);

        for (const account of accounts) {
            this.setQueryAccountNo(queryBuilder, account.accountNo);
        }
        return await queryBuilder.getMany();
    }

    async findTransactionByAccount(accountNo: string, findDto: FindTransactionDto): Promise<Transaction[]> {
        const queryBuilder = this.startQueryBuilder(findDto);
        this.setQueryAccountNo(queryBuilder, accountNo);

        return await queryBuilder.getMany();
    }

    private createAdjustmentTransactionData(adjustmentDto: SavingAdjusmentDto): Transaction {
        const transaction = this.repository.create();
        transaction.transactionId = uuid.v4();
        transaction.amount = adjustmentDto.amount;
        transaction.requesterAcctNo = adjustmentDto.accountNo;
        transaction.branchCreated = adjustmentDto.branchCode;
        transaction.createdBy = adjustmentDto.createdBy;
        return transaction;
    }

    private startQueryBuilder(findDto: FindTransactionDto): SelectQueryBuilder<Transaction> {
        const queryBuilder = getRepository(Transaction).createQueryBuilder('transaction');
        if (findDto.transactionType)
            queryBuilder.andWhere('transaction_type = :type', { type: findDto.transactionType });
        if (findDto.dateFrom)
            queryBuilder.andWhere('created_at >= :dateFrom', { dateFrom: new Date(findDto.dateFrom)});
        if (findDto.dateTo)
            queryBuilder.andWhere('created_at <= :dateTo', { dateTo: new Date(findDto.dateTo) });

        console.log(findDto.transactionType);
        return queryBuilder;
    }

    private setQueryAccountNo(queryBuilder: SelectQueryBuilder<Transaction>, accountNo: string): SelectQueryBuilder<Transaction> {
        return queryBuilder.andWhere('(requester_acct_no = :acctNo or destination_acct_no = :acctNo)', { acctNo: accountNo });
    }

    private async getCustomerAccounts(cifNo: string): Promise<any> {
        const customerUrl = this.configService.get<string>('customer.serviceUrl');
        const { data } = await axios.get(`${customerUrl}/api/customer/${cifNo}`);
        return await this.getAccounts(data.id);
    }

    private async getAccounts(customerId: number): Promise<any> {
        const accountUrl = this.configService.get<string>('account.serviceUrl');
        const { data } = await axios.get(`${accountUrl}/api/accounts/${customerId}/customer`);
        if (!data)
            return [];

        return data;
    }
}