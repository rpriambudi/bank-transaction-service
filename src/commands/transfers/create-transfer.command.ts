import { BaseCommand } from 'bank-shared-lib';

export class CreateTransferCommand extends BaseCommand {
    getCommandName(): string {
        return 'createTransferCommand';
    }
}