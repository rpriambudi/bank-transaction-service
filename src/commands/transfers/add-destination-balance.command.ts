import { BaseCommand } from 'bank-shared-lib';

export class AddDestinationBalanceCommand extends BaseCommand {
    getCommandName(): string {
        return 'addDestinationBalanceCommand';
    }
}