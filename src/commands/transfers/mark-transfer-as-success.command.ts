import { BaseCommand } from 'bank-shared-lib';

export class MarkTransferAsSuccessCommand extends BaseCommand {
    getCommandName(): string {
        return 'markTransferAsSuccessCommand';
    }

}