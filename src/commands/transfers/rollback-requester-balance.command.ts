import { BaseCommand } from 'bank-shared-lib';

export class RollbackRequesterBalanceCommand extends BaseCommand {
    getCommandName(): string {
        return 'rollbackRequesterBalanceCommand';
    }
    
}