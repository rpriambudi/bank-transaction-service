import { BaseCommand } from 'bank-shared-lib';

export class MarkTransferAsFailedCommand extends BaseCommand {
    getCommandName(): string {
        return 'markTransferAsFailedCommand';
    }

}