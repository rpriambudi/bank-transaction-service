import { BaseCommand } from 'bank-shared-lib';

export class DepositToBankAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'depositToBankAccountCommand';
    }
}