import { BaseCommand } from 'bank-shared-lib';

export class CreateDepositCommand extends BaseCommand {
    getCommandName(): string {
        return 'createDepositCommand';
    }
}