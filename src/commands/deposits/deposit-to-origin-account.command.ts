import { BaseCommand } from 'bank-shared-lib';

export class DepositToOriginAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'depositToOriginAccountCommand';
    }
}