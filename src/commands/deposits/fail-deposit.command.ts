import { BaseCommand } from 'bank-shared-lib';

export class FailDepositCommand extends BaseCommand {
    getCommandName(): string {
        return 'failDepositCommand';
    }
}