import { BaseCommand } from 'bank-shared-lib';

export class RollbackOriginDepositCommand extends BaseCommand {
    getCommandName(): string {
        return 'rollbackOriginDepositCommand';
    }
}