import { BaseCommand } from 'bank-shared-lib';

export class WithdrawFromBankAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'withdrawFromBankAccountCommand';
    }
}