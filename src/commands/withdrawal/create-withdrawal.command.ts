import { BaseCommand } from 'bank-shared-lib';

export class CreateWithdrawalCommand extends BaseCommand {
    getCommandName(): string {
        return 'createWithdrawalCommand';
    }
}