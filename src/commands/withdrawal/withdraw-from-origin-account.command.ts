import { BaseCommand } from 'bank-shared-lib';

export class WithdrawFromOriginAccountCommand extends BaseCommand {
    getCommandName(): string {
        return 'withdrawFromOriginAccountCommand';
    }
}