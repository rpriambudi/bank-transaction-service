import { BaseCommand } from 'bank-shared-lib';

export class FailWithdrawalCommand extends BaseCommand {
    getCommandName(): string {
        return 'failWithdrawalCommand';
    }
}