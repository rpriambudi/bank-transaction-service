import { BaseCommand } from 'bank-shared-lib';

export class RollbackOriginWithdrawalCommand extends BaseCommand {
    getCommandName(): string {
        return 'rollbackOriginWithdrawalCommand';
    }
}