import { Controller, Get, Param, Inject } from '@nestjs/common';
import { SagaService } from 'bank-shared-lib';

@Controller()
export class AppController {
  constructor(
    @Inject('SagaService') private readonly sagaService: SagaService
  ) {}

  @Get('/api/saga/state/:stateId')
  async findSagaState(
    @Param('stateId') stateId: number
  ) {
    return await this.sagaService.findSagaByStateId(stateId);
  }
}
