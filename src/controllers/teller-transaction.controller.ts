import { Controller, Post, Param, Body, Inject, Get, Query } from "@nestjs/common";
import { SavingAdjusmentDto } from "./../dto/saving-adjustmernt.dto";
import { FindTransactionDto } from './../dto/find-transaction.dto';
import { AuthContext, UserContext } from "bank-shared-lib";
import { WithdrawSaga } from "./../sagas/withdraw.saga";
import { DepositSaga } from './../sagas/deposit.saga';
import { TransactionService } from "./../interfaces/services/transaction-service.interface";

@Controller()
export class TellerTransactionController {
    constructor(
        @Inject('WithdrawSaga') private readonly withdrawSaga: WithdrawSaga,
        @Inject('DepositSaga') private readonly depositSaga: DepositSaga,
        @Inject('TransactionService') private readonly transactionService: TransactionService
    ) {}

    @Post('/api/transactions/withdraw/:accountNo')
    async requestWithdrawal(
        @AuthContext() userContext: UserContext,
        @Param('accountNo') accountNo: string,
        @Body() adjustmentDto: SavingAdjusmentDto
    ) {
        adjustmentDto.createdBy = userContext.email;
        adjustmentDto.accountNo = accountNo;
        adjustmentDto.branchCode = userContext.branchCode;
        
        return await this.withdrawSaga.startSaga(adjustmentDto, 'withdrawalStartedEvent');
    }

    @Post('/api/transactions/deposit/:accountNo')
    async requestDeposit(
        @AuthContext() userContext: UserContext,
        @Param('accountNo') accountNo: string,
        @Body() adjustmentDto: SavingAdjusmentDto
    ) {
        adjustmentDto.createdBy = userContext.email;
        adjustmentDto.accountNo = accountNo;
        adjustmentDto.branchCode = userContext.branchCode;
        
        return await this.withdrawSaga.startSaga(adjustmentDto, 'depositStartedEvent');
    }

    @Get('/api/transactions/:cifNo/customer')
    async findTransactionByCif(
        @Param('cifNo') cifNo: string,
        @Query() findDto: FindTransactionDto
    ) {
        return await this.transactionService.findTransactionByCif(cifNo, findDto);
    }

    @Get('/api/transactions/:accountNo/account')
    async findTransactionByAccount(
        @Param('accountNo') accountNo: string,
        @Query() findDto: FindTransactionDto
    ) {
        return await this.transactionService.findTransactionByAccount(accountNo, findDto);
    }
}