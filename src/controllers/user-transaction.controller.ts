import { Controller, Post, Param, Body, Inject, Get, Query } from "@nestjs/common";
import { AuthContext, UserContext } from "bank-shared-lib";
import { FindTransactionDto } from './../dto/find-transaction.dto';
import { UserVerificationDto } from './../dto/user-verification.dto';
import { UserTransferDto } from './../dto/user-transfer.dto';
import { UserAccountDto } from './../dto/user-account.dto';
import { RuleContext } from "./../implementations/rule-context.impl";
import { TransferSaga } from './../sagas/transfer.saga';
import { CustomerQuery } from './../queries/customer.query';
import { TransactionService } from './../interfaces/services/transaction-service.interface';

@Controller()
export class UserTransactionController {
    constructor(
        @Inject('UserTransferRule') private readonly userTransferRule: RuleContext,
        @Inject('CommonTransferRule') private readonly transferRule: RuleContext,
        @Inject('TransferSaga') private readonly transferSaga: TransferSaga,
        @Inject('CustomerQuery') private readonly customerQuery: CustomerQuery,
        @Inject('TransactionService') private readonly transactionService: TransactionService
    ) {}

    @Post('/api/transfer/customer')
    async createNewCustomerTransfer(
        @Body('transfer') userTransferDto: UserTransferDto,
        @Body('verification') verificationDto: UserVerificationDto,
        @AuthContext() userContext: UserContext
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(userContext.cifNo);
        const userAccountDto: UserAccountDto = {
            customerId: customer.id,
            customerAcctNo: userTransferDto.requesterAcctNo
        }
        await this.userTransferRule.validate(userAccountDto);

        userTransferDto.createdBy = userContext.email;
        userTransferDto.cifNo = userContext.cifNo;
        userTransferDto.branchCreated = userTransferDto.clientType;
        userTransferDto.userVerificationDto = verificationDto;
        await this.transferRule.validate(userTransferDto);

        return await this.transferSaga.startSaga(userTransferDto, 'transferStartedEvent');
    }

    @Get('/api/transactions/customer')
    async getCustomerTransactions(
        @AuthContext() userContext: UserContext,
        @Query() findDto: FindTransactionDto,
    ) {
        return await this.transactionService.findTransactionByCif(userContext.cifNo, findDto);
    }
}