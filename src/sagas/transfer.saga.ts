import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { TransferStartedEvent } from './../events/transfers/transfer-started.event';
import { CreateTransferCommand } from './../commands/transfers/create-transfer.command';
import { TransferCreatedEvent } from './../events/transfers/transfer-created.event';
import { SubstractRequesterBalanceCommand } from './../commands/transfers/substract-requester-balance.command';
import { RequesterBalanceSubstractedEvent } from './../events/transfers/requester-balance-substracted.event';
import { AddDestinationBalanceCommand } from './../commands/transfers/add-destination-balance.command';
import { DestinationBalanceAddedEvent } from './../events/transfers/destination-balance-added.event';
import { AddDestinationBalanceFailedEvent } from './../events/transfers/add-destination-balance-failed.event';
import { RollbackRequesterBalanceCommand } from './../commands/transfers/rollback-requester-balance.command';
import { RequesterBalanceRollbackedEvent } from './../events/transfers/requester-balance-rollbacked.event';
import { SubstractRequesterBalanceFailedEvent } from './../events/transfers/substract-requester-balance-failed.event';
import { MarkTransferAsSuccessCommand } from './../commands/transfers/mark-transfer-as-success.command';
import { TransferSuccessEvent } from './../events/transfers/transfer-success.event';
import { MarkTransferAsFailedCommand } from './../commands/transfers/mark-transfer-as-failed.command';
import { TransferFailedEvent } from './../events/transfers/transfer-failed.event';

export class TransferSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new TransferStartedEvent(new CreateTransferCommand()),
            new TransferCreatedEvent(new SubstractRequesterBalanceCommand()),
            new RequesterBalanceSubstractedEvent(new AddDestinationBalanceCommand()),
            new AddDestinationBalanceFailedEvent(new RollbackRequesterBalanceCommand()),
            new DestinationBalanceAddedEvent(new MarkTransferAsSuccessCommand()),
            new TransferSuccessEvent(null),
            new RequesterBalanceRollbackedEvent(new MarkTransferAsFailedCommand()),
            new SubstractRequesterBalanceFailedEvent(new MarkTransferAsFailedCommand()),
            new TransferFailedEvent(null)
        ];
    }
}