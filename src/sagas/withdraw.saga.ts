import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { WithdrawalStartedEvent } from './../events/withdraws/withdrawal-started.event';
import { CreateWithdrawalCommand } from './../commands/withdrawal/create-withdrawal.command';
import { WithdrawalCreatedEvent } from './../events/withdraws/withdrawal-created.event';
import { WithdrawFromOriginAccountCommand } from './../commands/withdrawal/withdraw-from-origin-account.command';
import { OriginAccountWithdrawnEvent } from './..//events/withdraws/origin-account-withdrawn.event';
import { WithdrawFromBankAccountCommand } from './../commands/withdrawal/withdraw-from-bank-account.command';
import { BankAccountWithdrawnEvent } from './../events/withdraws/bank-account-withdrawn.event';
import { WithdrawFromBankFailedEvent } from './../events/withdraws/withdraw-from-bank-failed.event';
import { FailWithdrawalCommand } from './../commands/withdrawal/fail-withdrawal.command';
import { RollbackOriginWithdrawalCommand } from './../commands/withdrawal/rollback-origin-withdrawal.command';
import { OriginWithdrawalRollbackedEvent } from './../events/withdraws/origin-withdrawal-rollbacked.event';
import { WithdrawalFailedEvent } from './../events/withdraws/withdrawal-failed.event';

export class WithdrawSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new WithdrawalStartedEvent(new CreateWithdrawalCommand()),
            new WithdrawalCreatedEvent(new WithdrawFromOriginAccountCommand()),
            new OriginAccountWithdrawnEvent(new WithdrawFromBankAccountCommand()),
            new BankAccountWithdrawnEvent(null),
            new WithdrawFromBankFailedEvent(new RollbackOriginWithdrawalCommand()),
            new OriginWithdrawalRollbackedEvent(new FailWithdrawalCommand()),
            new WithdrawalFailedEvent(null)
        ];
    }

}