import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { DepositStartedEvent } from './../events/deposits/deposit-started.event';
import { CreateDepositCommand } from './../commands/deposits/create-deposit.command';
import { DepositCreatedEvent } from './../events/deposits/deposit-created.event';
import { DepositToOriginAccountCommand } from './../commands/deposits/deposit-to-origin-account.command';
import { OriginAccountDepositedEvent } from './../events/deposits/origin-account-deposited.event';
import { DepositToBankAccountCommand } from './../commands/deposits/deposit-to-bank-account.command';
import { BankAccountDepositedEvent } from './../events/deposits/bank-account-deposited.event';
import { DepositToOriginFailedEvent } from './../events/deposits/deposit-to-origin-failed.event';
import { DepositToBankFailedEvent } from './../events/deposits/deposit-to-bank-failed.event';
import { RollbackOriginDepositCommand } from './../commands/deposits/rollback-origin-deposit.command';
import { OriginDepositRollbackedEvent } from './../events/deposits/origin-deposit-rollbacked.event';
import { FailDepositCommand } from './../commands/deposits/fail-deposit.command';
import { DepositFailedEvent } from './../events/deposits/deposit-failed.event';

export class DepositSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new DepositStartedEvent(new CreateDepositCommand()),
            new DepositCreatedEvent(new DepositToOriginAccountCommand()),
            new OriginAccountDepositedEvent(new DepositToBankAccountCommand()),
            new BankAccountDepositedEvent(null),
            new DepositToOriginFailedEvent(null),
            new DepositToBankFailedEvent(new RollbackOriginDepositCommand()),
            new OriginDepositRollbackedEvent(new FailDepositCommand()),
            new DepositFailedEvent(null)
        ]
    }

}