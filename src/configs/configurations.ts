export const AuthConfig = () => ({
    jwksUrl: process.env.KEYCLOAK_JWKS_URL,
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    adminUser: process.env.KEYCLOAK_ADMIN_USER,
    adminPassword: process.env.KEYCLOAK_ADMIN_PASSWORD,
    realmName: process.env.KEYCLOAK_REALM_NAME,
    baseUrl: process.env.KEYCLOAK_BASE_URL
})

export const DatabaseConfig = () => ({
    database: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        name: process.env.DB_NAME
    }
})

export const BrokerConfig = () => ({
    broker: {
        host: process.env.BROKER_HOST
    }
})

export const SystemConfig = () => ({
    branch: {
        clientCode: process.env.BRANCH_CLIENT_CODE
    }
})

export const ServiceConfig = () => ({
    account: {
        serviceUrl: process.env.ACCOUNT_SERVICE_URL
    },
    customer: {
        serviceUrl: process.env.CUSTOMER_SERVICE_URL
    }
})