import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

export enum TransactionStatus {
    OnProcess = 'ON_PROCESS',
    Failed = 'FAILED',
    Settled = 'SETTLED'
}

export enum TransactionTypeEnum {
    Transfer = 'TRF',
    Withdraw = 'DB',
    Deposit = 'CR'
}

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        name: 'transaction_id',
        unique: true
    })
    transactionId: string;

    @Column({
        type: 'varchar',
        name: 'transaction_type'
    })
    transactionType: string;

    @Column({
        type: 'varchar',
        name: 'transaction_status',
        default: TransactionStatus.OnProcess
    })
    transactionStatus: string;

    @Column({
        type: 'varchar',
        name: 'requester_acct_no'
    })
    requesterAcctNo: string;

    @Column({
        type: 'bigint',
        name: 'amount'
    })
    amount: number

    @Column({
        type: 'varchar',
        nullable: true,
        name: 'destination_acct_no'
    })
    destinationAcctNo: string

    @Column({
        type: 'text',
        name: 'description',
        nullable: true
    })
    description: string

    @Column({
        type: 'varchar',
        name: 'branch_created'
    })
    branchCreated: string;

    @Column({
        type: 'varchar',
        name: 'createdBy'
    })
    createdBy: string;

    @Column({
        type: 'timestamp',
        name: 'created_at',
        default: new Date()
    })
    createdAt: Date;
}