import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { Inject } from '@nestjs/common';
import { TransactionService } from './../../interfaces/services/transaction-service.interface';

export class CreateDepositHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }

    getCommandName(): string {
        return 'createDepositCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const transaction = await this.transactionService.requestDeposit(data);
        await this.brokerPublisher.dispatch('depositCreatedEvent', { stateId: data.stateId, transactionId: transaction.id });
        return;
    }
    
}