import { Inject } from "@nestjs/common";
import { BaseConsumer, IBrokerPublisher } from "bank-shared-lib";
import { TransactionService } from "./../../interfaces/services/transaction-service.interface";

export class MarkTransferAsSuccessHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }
    
    getCommandName(): string {
        return 'markTransferAsSuccessCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { transactionId, stateId } = data;
        await this.transactionService.setTransactionToSuccess(transactionId);
        await this.brokerPublisher.dispatch('transferFailedEvent', { stateId: stateId, transactionId: transactionId });
    }
    
}