import { Inject } from "@nestjs/common";
import { BaseConsumer, IBrokerPublisher } from "bank-shared-lib";
import { TransactionService } from "./../../interfaces/services/transaction-service.interface";

export class MarkTransferAsFailedHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }
    
    getCommandName(): string {
        return 'markTransferAsFailedCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { transactionId, stateId, errorMessage } = data;
        await this.transactionService.setTransactionToFail(transactionId);
        await this.brokerPublisher.dispatch('transferFailedEvent', { stateId: stateId, transactionId: transactionId, error: errorMessage });
    }
    
}