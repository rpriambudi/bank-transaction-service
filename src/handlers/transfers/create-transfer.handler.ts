import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { Inject } from '@nestjs/common';
import { TransactionService } from './../../interfaces/services/transaction-service.interface';

export class CreateTransferHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }

    getCommandName(): string {
        return 'createTransferCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const transfer = await this.transactionService.createNewTransfer(data);
        await this.brokerPublisher.dispatch('transferCreatedEvent', { stateId: data.stateId, transactionId: transfer.id });
        return;
    }
    
}