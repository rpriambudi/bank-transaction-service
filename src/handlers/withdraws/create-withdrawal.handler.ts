import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { Inject } from '@nestjs/common';
import { TransactionService } from './../../interfaces/services/transaction-service.interface';

export class CreateWithdrawalHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }

    getCommandName(): string {
        return 'createWithdrawalCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const transaction = await this.transactionService.requestWithdrawal(data);
        await this.brokerPublisher.dispatch('withdrawalCreatedEvent', { stateId: data.stateId, transactionId: transaction.id });
        return;
    }
    
}