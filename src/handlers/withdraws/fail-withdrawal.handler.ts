import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { Inject } from '@nestjs/common';
import { TransactionService } from './../../interfaces/services/transaction-service.interface';

export class FailWithdrawalHandler extends BaseConsumer {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService,
        @Inject('BrokerPublisher') private readonly brokerPublisher: IBrokerPublisher
    ) {
        super();
    }

    getCommandName(): string {
        return 'failWithdrawalCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, transactionId, errorMessage } = data;
        await this.transactionService.setTransactionToFail(transactionId);
        await this.brokerPublisher.dispatch('withdrawalFailedEvent', { stateId: stateId, error: errorMessage });
    }
    
}