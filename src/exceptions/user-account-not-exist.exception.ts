import { GenericException } from 'bank-shared-lib';

export class UserAccountNotExist extends GenericException {
    getErrorCode(): string {
        return '400904';
    }

    getDisplayCode(): string {
        return 'USER_ACCOUNT_NOT_EXIST';
    }

    constructor(message: string) {
        super(message);
    }
}