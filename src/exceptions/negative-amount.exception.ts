import { GenericException } from 'bank-shared-lib';

export class NegativeAmountException extends GenericException {
    getDisplayCode(): string {
        return 'NEGATIVE_AMOUNT';
    }

    getErrorCode(): string {
        return '400902';
    }

    constructor(message: string) {
        super(message);
    }
}