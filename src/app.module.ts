import { Module, NestModule, MiddlewareConsumer, Global } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BankSharedModule, SagaState } from 'bank-shared-lib';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { CustomerAuthMiddleware } from './middlewares/customer-auth.midlleware';
import { UserTransactionController } from './controllers/user-transaction.controller';
import { Transaction } from './entities/transaction.entity';
import { TransferSaga } from './sagas/transfer.saga';
import { TransactionServiceImpl } from './implementations/services/transaction-service.impl';
import { RuleContext } from './implementations/rule-context.impl';
import { TransferAccountRule } from './implementations/validations/transfer-account-rule.impl';
import { TransferGeneralRule } from './implementations/validations/transfer-general-rule.impl';
import { UserTransferRule } from './implementations/validations/user-transfer-rule.impl';
import { DatabaseConfig, ServiceConfig, AuthConfig } from './configs/configurations';
import { CreateTransferHandler } from './handlers/transfers/create-transfer.handler';
import { WithdrawSaga } from './sagas/withdraw.saga';
import { TellerAuthMiddleware } from './middlewares/teller-auth.middleware';
import { TellerTransactionController } from './controllers/teller-transaction.controller';
import { CreateWithdrawalHandler } from './handlers/withdraws/create-withdrawal.handler';
import { FailWithdrawalHandler } from './handlers/withdraws/fail-withdrawal.handler';
import { DepositSaga } from './sagas/deposit.saga';
import { CreateDepositHandler } from './handlers/deposits/create-deposit.handler';
import { FailDepositHandler } from './handlers/deposits/fail-withdrawal.handler';
import { CustomerQuery } from './queries/customer.query';
import { AccountQuery } from './queries/account.query';
import { AppController } from './app.controller';
import { MarkTransferAsSuccessHandler } from './handlers/transfers/mark-transfer-as-success.handler';
import { MarkTransferAsFailedHandler } from './handlers/transfers/mark-transfer-as-failed.handler';

const sharedSagaFactory = [
  {
    provide: 'TransferSaga',
    useClass: TransferSaga
  },
  {
    provide: 'WithdrawSaga',
    useClass: WithdrawSaga
  },
  {
    provide: 'DepositSaga',
    useClass: DepositSaga
  }
]

@Global()
@Module({
  imports: [
    BankSharedModule.registerSaga({
      useFactory: (repository, brokerPublisher) => ({
        sagaList: [
          new TransferSaga(repository, brokerPublisher),
          new WithdrawSaga(repository, brokerPublisher),
          new DepositSaga(repository, brokerPublisher)
        ]
      }),
      inject: [getRepositoryToken(SagaState), 'BrokerPublisher']
    }),
    BankSharedModule.registerHandler({
      useFactory: (transactionService, brokerPublisher) => ({
        commandList: [
          new CreateTransferHandler(transactionService, brokerPublisher),
          new CreateWithdrawalHandler(transactionService, brokerPublisher),
          new FailWithdrawalHandler(transactionService, brokerPublisher),
          new CreateDepositHandler(transactionService, brokerPublisher),
          new FailDepositHandler(transactionService, brokerPublisher),
          new MarkTransferAsSuccessHandler(transactionService, brokerPublisher),
          new MarkTransferAsFailedHandler(transactionService, brokerPublisher),
        ]
      }),
      inject: ['TransactionService', 'BrokerPublisher']
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        username: configService.get<string>('database.username'),
        password: configService.get<string>('database.password'),
        database: configService.get<string>('database.name'),
        entities: [SagaState],
        synchronize: true,
        autoLoadEntities: true
      }),
      inject: [ConfigService]
    }),
    TypeOrmModule.forFeature([Transaction]),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [DatabaseConfig, ServiceConfig, AuthConfig]
    })
  ],
  controllers: [UserTransactionController, TellerTransactionController, AppController],
  providers: [
    {
      provide: 'TransactionService',
      useClass: TransactionServiceImpl
    },
    {
      provide: 'CommonTransferRule',
      useFactory: () => {
        return new RuleContext([
          new TransferAccountRule(),
          new TransferGeneralRule()
        ]);
      }
    },
    {
      provide: 'UserTransferRule',
      useFactory: (configService) => {
        return new RuleContext([new UserTransferRule(configService)])
      },
      inject: [ConfigService]
    },
    {
      provide: 'CustomerQuery',
      useFactory: (configService) => {
        return new CustomerQuery(configService);
      },
      inject: [ConfigService]
    },
    {
      provide: 'AccountQuery',
      useFactory: (configService) => {
        return new AccountQuery(configService);
      },
      inject: [ConfigService]
    },
    ...sharedSagaFactory
  ],
  exports: [
    {
      provide: 'TransactionService',
      useClass: TransactionServiceImpl
    },
    ...sharedSagaFactory
  ]
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CustomerAuthMiddleware)
      .forRoutes(UserTransactionController)

    consumer
      .apply(TellerAuthMiddleware)
      .forRoutes(TellerTransactionController)
  }
}
