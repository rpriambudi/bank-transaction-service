# Base OS
FROM node:10-alpine

# Working directory
WORKDIR /usr/src/bank-customer-service

COPY package*.json ./

RUN apk add --no-cache bash

RUN npm install

COPY . .

EXPOSE 3005

CMD ["npm", "run", "start:dev"]
